print('Hello Twist Project')
local platform = require("platform")
local btnFactory = require("roundrectbutton_factory")
local circleFactory = require("circleplatform_factory")
local lineFactory = require("line_factory")
local button = btnFactory:New()
local circle = circleFactory:New()
button.x = 320
button.y = 700
button.width = 110
button.height = 200
circle.x = 320
circle.y = 500
platform:SetButton(button)
platform:SetCircle(circle)
platform:AddLine(lineFactory:New())
platform:Display()
