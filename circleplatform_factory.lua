--[[----------------------------
CirclePlatformFactory
- Factory for Circle Platform (Background)
--------------------------------]]

local TWCirclePlatformFactory = {}
TWCirclePlatformFactory_mt = {__index = TWCirclePlatformFactory}


-- Create new dummy RoundRect Button
function TWCirclePlatformFactory:New()
	local circle = display.newCircle(0,0,300)
	circle.isVisible = false
	return circle
end


return TWCirclePlatformFactory

