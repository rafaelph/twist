--[[----------------------------
CirclePlatformFactory
- Factory for Circle Platform (Background)
--------------------------------]]

local TWLineFactory = {}
TWLineFactory_mt = {__index = TWLineFactory}


-- Create new dummy RoundRect Button
function TWLineFactory:New(listener)
	local line = display.newRect(320,500,5,300)
	line:setFillColor(0,1,0,1)
	line.rotate = {}
	line.rotate.iterations = -1
	line.rotate.time = 1000
	line.rotate.rotation = 360
	line.rotate.listener = listener or nil
	line.isVisible = false
	line.anchorY = 0
	return line
end


return TWLineFactory

