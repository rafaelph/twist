--[[-------------------
TWPlatorm Module
- Module for initializing and generating the stage platform
for Twist Game
- Add additional rotating lines
- Display the stage
---------------------]]
local TWPlatform = {}
TWPlatform_mt = {__index = TWPlatform}


--[[-------------
Private Variables
---------------]]

-- Circle Stage
local _circle = nil
-- Circle Parameters
local _circleRadius = 300
local _centerX = 320
local _centerY = 500
-- Button to Press
local _button = nil
-- Button Paramters
local _isPressed = false
local _buttonWidth = 110
local _buttonHeight = 200
local _buttonRadius = 50
local _buttonListener = nil

-- Rotating Lines
local _lines = {}
-- If Displayed
local _isDisplayed = false

-- Private Functions
local function SetVisibility(obj)
	for i = 1,#obj do
		obj[i].isVisible = true
	end
end

local function StartRotation(lines)
	for i = 1,#lines do
		lines[i].isVisible = true
		transition.to(lines[i],lines[i].rotate)
	end
end


-- Public Functions
-- Call this to display the platform
function TWPlatform:Display()
	print('Initializing Platform')
	_circle = _circle or display.newCircle(_centerX,_centerY,300)
	_circle:setFillColor(0,0.8,0,0.5)
	_button = _button or display.newRoundedRect(_centerX,_centerY+200,_buttonWidth,_buttonHeight,_buttonRadius)
	_button.alpha = 0.2
	-- Add listener
	_buttonListener = _buttonListener or function(event)
			if(event.phase == "began") then
				print('Looool')
			end
		end
	_button:addEventListener("touch",_buttonListener)
	_button.isVisible = true
	_circle.isVisible = true
	SetVisibility(_lines)
	StartRotation(_lines)

end

function TWPlatform:AddLine(line)
	if not line then return end
	print('Adding line')
	print(line.rotate.time)
	table.insert(_lines, line)
end


function TWPlatform:SetButton(newButton)
	_button = newButton
end

function TWPlatform:SetCircle(newCircle)
	_circle = newCircle
end

function TWPlatform:SetLines(lines)
	_lines = nil
	_lines = lines
end

function TWPlatform:Clear()
	_circle = nil
	_button = nil
	_lines = {}
end

return TWPlatform