--[[----------------------------
RoundRectButtonFactory
- Factory for RoundRect buttons
- Has no default size (but given radius due
PRO restriction)
--------------------------------]]

local TWRoundRectButtonFactory = {}
TWRoundRectButtonFactory_mt = {__index = TWRoundRectButtonFactory}


-- Create new dummy RoundRect Button
function TWRoundRectButtonFactory:New()
	local btn = display.newRoundedRect(0,0,0,0,50)
	btn.isVisible = false
	return btn
end


return TWRoundRectButtonFactory

